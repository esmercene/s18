console.log("Hello Friend");

function sumOfTwoNumbers(num1,num2){
	let sum = num1 + num2;
	console.log("Displayed sum of",num1,"and",num2);
	console.log(sum);
}
sumOfTwoNumbers(4,3);

function differenceOfTwoNumbers(num1,num2){
	let difference = num1 - num2;
	console.log("Displayed difference of",num1,"and",num2);
	console.log(difference);
}
differenceOfTwoNumbers(4,3);



function productOfTwoNumbers(num1,num2){
	let productOfNum = num1 * num2;
	console.log("The product of",num1,"and",num2,":");
	return productOfNum;
};
let product = productOfTwoNumbers(4,3);
console.log(product);


function quotientOfTwoNumbers(num1,num2){
	let quotientOfNum = num1 / num2;
	console.log("The quotient of",num1,"and",num2,":");
	return quotientOfNum;
};
let quotient = quotientOfTwoNumbers(4,3);
console.log(quotient);


function areaOfCircle(radius){
	let areaFormula = (Math.PI * (radius ** 2));
	console.log("The result of getting the area of a circle with",radius,"radius:");
	return areaFormula;
};
let circleArea = areaOfCircle(10);
console.log(circleArea);



function totalAverageNumbers(num1,num2,num3,num4){
	let averageNumbers = ((num1 + num2 + num3 + num4) / 4);
	console.log("The average of",num1 + "," + num2 +"," + num3 + "," + num4 + ":");
	return averageNumbers;
};
let averageVar = totalAverageNumbers(5,8,10,9);
console.log(averageVar);




function checkPercentageScore(num1,num2){
	let percentageScore = (num1/num2) * 100;
	let passingScore = percentageScore > 75;
	console.log("Is",num1 + "/" + num2,"a passing score?");
	return passingScore;
};
let isPassingScore = checkPercentageScore(35,60);
console.log(isPassingScore);
















